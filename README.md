# Diabeat-it

## Description
For many diabetic patients, individual care and management is extremely difficult. The
Diabeat-it app simplifies the process of determining the correct treatment options for diabetic patients. They are able to upload their glucose levels, activities and diets, and Diabeat-it will compile that data in one place and use that data combined with information from medical professionals to come up with a treatment plan.

## Usage
This application will be a web app hosted on localhost:3000 with a web server on localhost:5000 and a database on localhost:27017

## Authors and acknowledgment
SE3810 021 Group 3

## License
For open source projects, say how it is licensed.

## Project status
Good for demo
