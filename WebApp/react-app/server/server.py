import datetime
import random
from datetime import datetime
from pymongo import MongoClient
from flask import Flask, request

# Connect to Mongo db
mongoClient = MongoClient('localhost', 27017)
print('Connected to Mongo server')

# Connect to Diabeat-it database
diabeatDb = mongoClient['Diabeat-it']  # Dictionary style access because of the - in name
print('Connected to db')

# Getting both collections in db
user_login = diabeatDb.user_login
patient_data = diabeatDb.patient_data
print('Got collections')

app = Flask(__name__)

users = {"test" : "pass"} # a dict is used in the temporary absense of a database

user_logged_in = False

@app.route('/')
def hello_world():
    return "Hello, World"

@app.route('/signup', methods = ['GET', 'POST'])
def user_signup():
    (username, password) = (request.args.get('name'), request.args.get('pass'))
    users[username] = password
    return "Name added"

@app.route('/login', methods=['POST'])
def user_loggin_in():
        
    username = request.json.get("email", None)
    password = request.json.get("password", None)
    token = request.json.get("token", None)

    print (username)
    print (password)

    valid = 50

    if(token is None):
        valid = verify_user_login(username, password)
    else: 
        valid = verify_user_login(username, password, token)

    if(valid == 1):
        user = user_login.find_one({'username': {'$eq': username}})
        return {
            "success": "true",
            "token": user['token'],
            "expiration": user['expiration']
        }
    else:
        return "false"

def verify_user_login(username, password, token=-1):
    user = user_login.find_one({'username': {'$eq': username}})

    if user is None:  # Nonexistent user
        return -1
    elif (user_has_token(username)) and (verify_user_token(username, token) == 1):  # Has a valid and verified token
        return 1
    else:  # Login with password
        success = verify_user_password(username, password)  # 0 or one based on successful login
        if success == 1:
            update_security_token(username)

        return success


# Returns true or false if the user has a valid token
def user_has_token(username):
    user = user_login.find_one({'username': {'$eq': username}})

    if (user["token"] != -1) and (user["expiration"] > datetime.now()):
        return True
    else:
        return False


# Returns 0 if invalid token, 1 if valid and not expired
def verify_user_token(username, token):
    user = user_login.find_one({'username': {'$eq': username}})

    if token == -1:
        return 0
    elif (user["token"] == token) and (user["expiration"] > datetime.now()):
        return 1
    else:
        return 0


def verify_user_password(username, password):
    user = user_login.find_one({'username': {'$eq': username}})

    if user["password"] == password:
        return 1  # Correct password
    else:
        return 0  # Incorrect password


# endregion

# region Set Data
def add_smbg_level(username, level, date):
    if user_login.find_one({'username': {'$eq': username}}) is None:
        return -1
    else:
        objectid = patient_data.insert_one({
            "username": username,
            "smbg_level": level,
            "time_taken": date
        }).inserted_id
        return objectid


def update_security_token(username):
    pair = generate_token()
    user_login.find_one_and_update({'username': {'$eq': username}},
                                   {'$set': {'token': pair[0], 'expiration': pair[1]}}
                                   )

def add_user(username, token, expiration):
    user_login.insert_one({
        'username': username,
        'token': token,
        'expiration': expiration
    })


# region Get Data
def get_all_patient_data(username):
    cursor = patient_data.find({'username': {'$eq': username}})
    return list(cursor)


def get_all_smbg(username):
    smbg = []
    listdata = get_all_patient_data(username)
    for d in listdata:
        smbg.append(d["smbg_level"])
    return smbg
    
# This method generates a token and a datetime object that represents the expiration time for the token and returns them
def generate_token():
    token = random.randint(0, 1000)
    temp = datetime.now()

    hour = temp.hour
    minute = temp.minute + 5
    if minute > 60:
        minute -= 60
        hour += 1

    expiration = datetime(temp.year, temp.month, temp.day, hour, minute)

    return token, expiration


if __name__ == "__main__":
    app.run()