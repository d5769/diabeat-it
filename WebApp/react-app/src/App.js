import React, {useEffect, useState, useContext} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
}
from "react-router-dom";
import './App.css';
import { createBrowserHistory } from 'history';
import {Button, Form, Container, Card} from 'react-bootstrap';
import axios from "axios"
// import LogIn from "./pages/LogIn";
// import LogInSuccess from "./pages/LogInSuccess";

function App(props) {

    const [state, setState] = useState({
        email: "",
        password: "",
        token: -1
    });

    const handleChange = (e) => {
        const {name, value} = e.target;
        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    function logIn(event){
        if(state.token == -1) {
            axios({
                method: "POST",
                url: "/login",
                data: {
                    email: state.email,
                    password: state.password
                }
            })
                .then((response) => {
                    console.log(response);
                    state.token = response.data.token;
                    console.log(state.token);
                    //props.setToken(response.data.access_token);
                }).catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                }
            })
        } else {
            axios({
                method: "POST",
                url: "/login",
                data: {
                    email: state.email,
                    token: state.token
                }
            })
                .then((response) => {
                    console.log(response);
                    console.log(state.token);
                    //props.setToken(response.data.access_token);
                }).catch((error) => {
                if (error.response) {
                    console.log(error.response);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                }
            })
        }
    }

    /**
     Logs in a user, after success it will direct to the home page again.
     */
    // const handleSubmitClick = async (e) => {
    //     e.preventDefault();
    //     const payload = {
    //         strategy: 'local',
    //         email: state.email,
    //         password: state.password,
    //     };
    // }
    
  return (
    // <Router history={createBrowserHistory()}>  
    // <div className="App">
    //   <Switch>
    //       <Route path={"/login"} exact component={LogIn}/>
    //       <Route path={"/loggedin"} exact component={LogInSuccess}/>
    //   </Switch>
    // </div>
    // </Router>
        <div className={'textFont'}>
            <Container className={'my-3 d-flex justify-content-center'}>
                <Card className={"w-50"}>
                    <Card.Header className={'text-center cardHeader'}><h3>Log in</h3></Card.Header>
                    <Card.Body>
                        <Form>
                            <Form.Group controlId={"formBasicEmail"}>
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type={'email'} placeholder={'Enter email'} name={'email'}
                                              value={state.value}
                                              onChange={handleChange} required/>
                            </Form.Group>
                            <Form.Group controlId={"formBasicPassword"}>
                                <Form.Label>Password</Form.Label>
                                <Form.Control type={'password'} placeholder={'Enter password'} name={'password'}
                                              value={state.value} onChange={handleChange} required/>
                            </Form.Group>
                        </Form>
                        <Button variant='primary' type={'submit'} onClick={logIn}>Log in</Button>
                        <div style={{display:'flex', justifyContent:'left', alignItems:'center', gap:'1ch'}}><p className={'mt-3'}> Not a member? </p>Sign up</div>
                    </Card.Body>
                </Card>
            </Container>
        </div>
  );
}

export default App;
