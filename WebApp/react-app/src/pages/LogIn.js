import React, {useState, useContext} from 'react';
import {useUser} from "./utils/UserContext";
import {Button, Form, Container, Card} from 'react-bootstrap';
import {app} from './utils/ServerConnection'
import {Link} from "react-router-dom";


function LogIn() {

    const {user,setAuthUser} = useUser();

    const [state, setState] = useState({
        email: "",
        password: "",
    });

    const handleChange = (e) => {
        const {name, value} = e.target;
        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    /**
     Logs in a user, after success it will direct to the home page again.
     */
    const handleSubmitClick = async (e) => {
        e.preventDefault();
        const payload = {
            strategy: 'local',
            email: state.email,
            password: state.password,
        };

        /* Tries to authenticate with feathers and sets auth token state to use the user's JWT
           from the authenticate service
         */
        try {
            const {authedUser} = await app.authenticate(payload);
            setAuthUser(authedUser);
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <div className={'textFont'}>
            <Container className={'my-3 d-flex justify-content-center'}>
                <Card className={"w-50"}>
                    <Card.Header className={'text-center cardHeader'}><h3>Log in</h3></Card.Header>
                    <Card.Body>
                        <Form>
                            <Form.Group controlId={"formBasicEmail"}>
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type={'email'} placeholder={'Enter email'} name={'email'}
                                              value={state.value}
                                              onChange={handleChange} required/>
                            </Form.Group>
                            <Form.Group controlId={"formBasicPassword"}>
                                <Form.Label>Password</Form.Label>
                                <Form.Control type={'password'} placeholder={'Enter password'} name={'password'}
                                              value={state.value} onChange={handleChange} required/>
                            </Form.Group>
                        </Form>
                        <Button variant='primary' type={'submit'} onClick={handleSubmitClick}><Link style={{color:'white', textDecoration:'none'}} to={'/'}>Log in</Link></Button>
                        <div style={{display:'flex', justifyContent:'left', alignItems:'center', gap:'1ch'}}><p className={'mt-3'}> Not a member? </p><Link to={"/signup"}>Sign up</Link></div>
                    </Card.Body>
                </Card>
            </Container>
        </div>

    )
}

export default LogIn;
