//This is used to store the user data when they log in (react hook: useContext) - Jon
import {createContext, useContext, useState, useEffect} from 'react';
import {app} from './ServerConnection'


const UserContext = createContext(null);

export const useUser = () => {
    return useContext(UserContext);
}

export const UsersProvider = ({ children }) => {
    const [user, setUser] = useState(null);

    useEffect(() => {
        app.reAuthenticate()
            .then(({user}) => {
                setUser(user);
            })
            .catch(() => {
                //Do nothing, user was not logged in previously
            });
    }, [user]);

    const getUser = () => {
        return user;
    }

    const setAuthUser = (authUser) => {
        setUser(user => authUser)
    }

    return (
        <UserContext.Provider
            value={{
                user,
                setAuthUser,
                getUser
            }}
        >
            {children}
        </UserContext.Provider>
    )
}