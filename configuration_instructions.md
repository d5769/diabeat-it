# Setup Instructions

## What to have installed
Since we do not have any sort of Docker image or virtual environment, it will be up to the user to have all required dependencies.  It is required that the user have python,
MongoDB, and npm installed before executing any of these instructions

## Configuration commands
In no particular order, the following libraries will have to be installed:

Server dependencies:
```bash
$ pip install flask python-dotenv pymongo
```

axios:
```bash
$ npm install axios
```

## Database Configuration
It is necessary that the data contained in the `raw` folder is contained within the local Mongo database of the user.  We did this with Mongo Compass, but however you can figure it out is fine.

============================
# Execution Instructions
First, get a terminal instance running in the react-app directory:
```bash
$ cd WebApp/react-app
```

Then, run the server with:
```bash
$ npm run start-server
```

With another terminal instance in the same location, start the client with:
```bash
$ npm start
```

All important output should be logged to the console